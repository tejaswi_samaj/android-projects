package com.android.dynamicui45;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    EditText userNametEditText = null;
    EditText passwordEditText = null;
    TextView summaryTextView = null;
    LayoutParams viewLayoutParams = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//Here we define parameters for views
        viewLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        viewLayoutParams.leftMargin = 40;
        viewLayoutParams.rightMargin = 40;
        viewLayoutParams.topMargin = 10;
        viewLayoutParams.bottomMargin = 10;
//Here we create the layout
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
//Here we define a text view
        TextView userNameTextView = new TextView(this);
        userNameTextView.setText("User name");
        userNameTextView.setLayoutParams(viewLayoutParams);
        linearLayout.addView(userNameTextView);
//Here we define the edit text
        userNametEditText = new EditText(this);
        userNametEditText.setLayoutParams(viewLayoutParams);
        userNametEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((EditText) v).setBackgroundColor(getResources().getColor(android.R.color.background_light));
                return false;
            }
        });
        linearLayout.addView(userNametEditText);
//Here we define a text view
        TextView passwordTextView = new TextView(this);
        passwordTextView.setText("Paasword");
        passwordTextView.setLayoutParams(viewLayoutParams);
        linearLayout.addView(passwordTextView);
//Here we define the edit text
        passwordEditText = new EditText(this);
        passwordEditText.setLayoutParams(viewLayoutParams);
        passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
//Here we hide the content of the password field
        passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((EditText) v).setBackgroundColor(getResources().getColor(android.R.color.background_light));
                return false;
            }
        });
        linearLayout.addView(passwordEditText);
        Button loginButton = new Button(this);
        loginButton.setText("Login");
        loginButton.setLayoutParams(viewLayoutParams);
        loginButton.setOnClickListener(buttonClickListener);
        linearLayout.addView(loginButton);
//Here we define a text view
        summaryTextView = new TextView(this);
        summaryTextView.setLayoutParams(viewLayoutParams);
        linearLayout.addView(summaryTextView);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        this.addContentView(linearLayout, linearLayoutParams);
    }

    private View.OnClickListener buttonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean feedback = false;
            if (userNametEditText.getText().length() == 0) {
                userNametEditText.setBackgroundColor(Color.rgb(254, 150, 150));
                feedback = true;
            }
            if (passwordEditText.getText().length() == 0) {
                passwordEditText.setBackgroundColor(Color.rgb(254, 150, 150));
                feedback = true;
            }
            if (feedback)
                Toast.makeText(getBaseContext(), "Missing data!", Toast.LENGTH_SHORT).show();
            else
                summaryTextView.setText(userNametEditText.getText().toString() + "\n" + passwordEditText.getText().toString());
        }
    };
}
