package com.android.simplemenu13;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This
     * @param menu
     * @return True - to show menu
     * False - Not to show menu
     * Super class always return True
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub

        menu.add("Sign In");
        menu.add("Account Settings");
        menu.add("Privacy");
        menu.add("Sign Out");

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Which menu item is clicked
     * @param item
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        Toast.makeText(this,item.getTitle()+ "Clicked",Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }
}
