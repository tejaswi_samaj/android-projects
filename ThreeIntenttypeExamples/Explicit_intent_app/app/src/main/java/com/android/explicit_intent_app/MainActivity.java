package com.android.explicit_intent_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button1=(Button)findViewById(R.id.Button01);

        button1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ActivityTwo.class);
                i.putExtra("Value1", "By ALICE");
                i.putExtra("Value2", "BY BOB");
                // Set the request code to any code you like, you can identify the
                // callback via this code
                startActivity(i);
            }
        });
    }
}

