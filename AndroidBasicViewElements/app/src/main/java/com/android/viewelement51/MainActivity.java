package com.android.viewelement51;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends ActionBarActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener, View.OnKeyListener{

    String feedbackText = "";
    String favorites = "";
    String gendre = "";
    String accepted = "";
    boolean imageSelected = false;


    private void displayToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Here we access the UI feedbackEditTex view
        EditText feedbackEditTex = (EditText) findViewById(R.id.et_feedback);
// Here we add listener to the view feedbackEditTex.setOnKeyListener(this);
// Here we access the UI sportsCheckBox view
        CheckBox sportsCheckBox = (CheckBox) findViewById(R.id.chkbox_sports);
// Here we add listener to the view sportsCheckBox.setOnClickListener(this);
// Here we access the UI readingCheckBox view
        CheckBox readingCheckBox = (CheckBox) findViewById(R.id.chkbox_reading);
// Here we add listener to the view readingCheckBox.setOnClickListener(this);
// Here we access the UI genderRadioGroup view
        RadioGroup genderRadioGroup = (RadioGroup) findViewById(R.id.radio_group_gender);
// Here we add listener to the view genderRadioGroup.setOnCheckedChangeListener(this);
// Here we access the UI acceptToggleButton view
        ToggleButton acceptToggleButton = (ToggleButton) findViewById(R.id.toggle_btn_accept);
// Here we add listener to the view acceptToggleButton.setOnClickListener(this);
// Here we access the UI imageButton view
        ImageButton imageButton = (ImageButton) findViewById(R.id.btn_img);
        imageButton.setImageResource(R.drawable.btn_img_img1);
// Here we add listener to the view imageButton.setOnClickListener(this);
// Here we access send button and define a listener for it
        Button sendButton = (Button) findViewById(R.id.btn_send);
// Here we add listener to the view sendButton.setOnClickListener(this);
        Button cancelButton = (Button) findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present. getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof CheckBox) {
            CheckBox checkBox = (CheckBox) v;
            displayToast(checkBox.getText().toString() + " " + getString(R.string.checked_fb));
            favorites = checkBox.getText().toString();
        } else if (v instanceof Button) {
            Button clickedButton = (Button) v;
            if (clickedButton instanceof ToggleButton) {
                ToggleButton toggleButton = (ToggleButton) clickedButton;
                displayToast(toggleButton.getText() + " " + getString(R.string.clicked_fb));
            } else {
                displayToast(clickedButton.getText() + " " + getString(R.string.clicked_fb));

            }
        } else if (v instanceof ImageButton) {
            ImageButton imageButton = (ImageButton) v;
            if (imageButton.getTag().toString().equals("image1")) {
                ImageButton tempImageButton = (ImageButton) findViewById(R.id.btn_img);
                tempImageButton.setImageResource(R.drawable.btn_img_img2);
                imageButton = tempImageButton;
                imageButton.setTag("image2");
            } else {
                ImageButton tempImageButton = (ImageButton) findViewById(R.id.btn_img);
                tempImageButton.setImageResource(R.drawable.btn_img_img1);
                imageButton = tempImageButton;
                imageButton.setTag("image1");
            }
            displayToast(imageButton.getTag().toString() + " " + getString(R.string.clicked_fb));
        }
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (view instanceof EditText) {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                displayToast(getString(R.string.fb_txt_ready));
            }
        }
        return false;
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        RadioButton radioButton = (RadioButton) findViewById(radioGroup .getCheckedRadioButtonId());
        displayToast(radioButton.getText() + " " + getString(R.string.selected_fb));
    }
}

